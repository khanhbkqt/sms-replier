package com.kasoft.smsreplier;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;

import java.util.Arrays;

/**
 * Created by Khanh on 1/10/2016.
 */
public class SmsListener extends BroadcastReceiver {

    @SuppressWarnings("deprecation")
    public void onReceive(Context context, Intent intent)
    {
        Bundle myBundle = intent.getExtras();
        String strMessage = "";

        if (myBundle != null)
        {
            Object [] pdus = (Object[]) myBundle.get("pdus");

            for (int i = 0; i < pdus.length; i++)
            {

                SmsMessage message;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    String format = myBundle.getString("format");
                    message = SmsMessage.createFromPdu((byte[]) pdus[i], format);
                }
                else {
                    message = SmsMessage.createFromPdu((byte[]) pdus[i]);
                }
                strMessage += "SMS From: " + message.getOriginatingAddress();
                strMessage += " : ";
                strMessage += message.getMessageBody();
                strMessage += "\n";

                Log.d("Sms Receiver", strMessage);

                handleIncomingSms(message);
            }
        }
    }

    private void handleIncomingSms(SmsMessage smsMessage) {

        String body = smsMessage.getMessageBody();
        String parts[] = body.split("\\.");

        Log.d("Sms Receiver", Arrays.toString(parts));

        for (String part : parts) {

            Log.d("Sms Receiver", "Processing: " + part);

            part = part.trim();

            if (part.startsWith("Reply '") && part.endsWith("' to claim")) {

                Log.d("Sms Receiver", part + " matches condition");

                String replyBody = getReplyBody(part);

                if (!(replyBody == null)) {
                    String phoneNumber = smsMessage.getOriginatingAddress();
                    sendSms(phoneNumber, replyBody, false);
                    abortBroadcast();
                    Log.d("Sms Receiver", "Sms sent to " + phoneNumber + " - " + replyBody);
                }
            }
        }
    }

    private String getReplyBody(String text) {
        String parts[] = text.split("'");
        for (String part : parts) {
            if (part.startsWith("YES")) {
                return part;
            }
        }
        return null;
    }

    private void sendSms(String phoneNumber, String message, boolean isBinary) {
        SmsManager.getDefault().sendTextMessage(phoneNumber, null, message, null, null);
    }
}
