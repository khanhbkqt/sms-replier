package com.kasoft.smsreplier;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

/**
 * Created by Khanh on 1/10/2016.
 */
public class SmsActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        findViewById(R.id.btnMinimizeApp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                minimizeApp();
            }
        });
    }

    private void minimizeApp() {
        moveTaskToBack(true);
    }
}
